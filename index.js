const { DB, PORT } = require('./config/index')
const passport = require('passport');
const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');
const { connect } = require('mongoose');
const session = require('express-session');
const app = express();

const userRoutes = require('./routes/user');
const authRoutes = require('./routes/auth');

require('./middlewares/passport')(passport)
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());

app.use(session({
  secret: 'asil',
  resave: true,
  saveUninitialized: true,
}));
app.use(function (req, res, next) {
  next();
});

passport.serializeUser(function (user, cb) {
  cb(null, user);
});

passport.deserializeUser(function (obj, cb) {
  cb(null, obj);
});

app.use("/auth", authRoutes);
app.use("/user", userRoutes);

const startApp = async () => {
  console.log(DB)
  try {
    await connect(DB, {
      useFindAndModify: true,
      useUnifiedTopology: true,
      useNewUrlParser: true
    });
    console.log(`Mongodb succesfully connectes`)
    app.listen(PORT, () => console.log(`Server Started ${PORT}`))
  } catch (error) {
    console.log(error)
  }
}

startApp()

