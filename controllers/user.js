const User = require('../models/User');
const getAllUser = async (req, res) => {
    const user = await User.find({}).select("_id, email")
    return res.status(200).json(user);
}

const getUserById = async (req, res) => {
    const user = await User.findById(req.params.id);
    return res.status(200).json(user);
}

const deleteUser = async (req, res) => {
    await User.findByIdAndRemove({ _id: req.params.id });
    res.status(200).json({
        message: 'User is  removed'
    });
}


module.exports = { getAllUser, getUserById, deleteUser }
