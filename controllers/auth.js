const jwt = require('jsonwebtoken');
const passport = require('passport');
const User = require('../models/User');
const bcrypt = require('bcryptjs');

const userRegister = async (userData, req, res) => {
    try {
        let emailNotTaken = await validateEmail(userData.email);
        if (!emailNotTaken) {
            return res.status(400).json({ message: `Email already taken` });
        }
        const password = await bcrypt.hash(userData.password, 12);
        const newUser = new User({
            email: userData.email,
            password: password,
        })
        await newUser.save()
        res.status(200).json(newUser)

    } catch (error) {
        return res.status(500).json({
            message: `Unable to create account`
        })
    }

}

const userLogin = async (userData, role, res) => {
    let { username, password } = userData;
    const user = await User.findOne({ username });
    if (!user) {
        return res.status(400).json({
            message: 'User not found'
        })
    }
  
    let isMatch = await bcrypt.compare(password, user.password);

    if (isMatch) {
        let token = jwt.sign({
            user_id: user.id,
            email: user.email,
        }, 'qwerty', { expiresIn: '7 days' });

        let result = {
            token: `Bearer ${token}`,
        }
        return res.status(200).json({
            ...result
        })
    } else {
        return res.status(400).json({
            message: 'Incorrect password'
        })
    }
}

const userAuth = passport.authenticate('jwt', { session: false });


const serializeUser = user => {
    return {
        username: user.username,
        email: user.email,
        _id: user._id,
        phoneNumber: user.phoneNumber,
        image: user.image,
        role: user.role,
        cardNumber: user.cardNumber,
        cardType: user.cardType,
        cardUsername: user.cardUsername,
        cardTime: user.cardTime
    }
}

const checkRole = roles => (req, res, next) => !roles.includes(req.user.role) ? res.status(400).json({
    message: 'Unathorized'
}) : next();


const validateEmail = async email => {
    let user = await User.findOne({ email });
    console.log(user)
    return user ? false : true
}

const refreshToken = async (token, req, res) => {
    if (token.expiresIn) {
        var decoded = jwt.decode(token, { complete: true });
        if (decoded.user_id == req.session.userId) {
            let newtoken = jwt.sign({
                user_id: decoded.user_id,
                role: decoded.role,
                email: decoded.email,
                username: decoded.username
            }, 'qwerty', { expiresIn: '7 days' });
            let result = {
                username: decoded.username,
                role: decoded.role,
                email: decoded.email,
                token: `Bearer ${newtoken}`,
                expiresIn: 168
            }
            return res.status(400).json({
                ...result,
                message: 'succesfully refresh'
            })
        }


    }
}

module.exports = { userAuth, userRegister, userLogin, serializeUser, checkRole, refreshToken }