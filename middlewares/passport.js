const { SECRET } = require('../config/index');
const { ExtractJwt, Strategy } = require('passport-jwt');
const User = require('../models/User');
const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: SECRET
}

module.exports = passport => {
  passport.use(new Strategy(opts, async (payload, done) => {
    await User.findById(payload.user_id).then(async user => {
      if (user) {
        return done(null, user)
      } else {
        return done(null, false)
      }
    }).catch(err => {
      return done(null, false)
    })
  }))
} 