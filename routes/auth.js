const router = require("express").Router();

const { userRegister, userLogin, userAuth, serializeUser, refreshToken } = require('../controllers/auth');

router.post("/register", async (req, res) => {
    await userRegister(req.body, req, res);
})

router.post("/login", async (req, res) => {
    await userLogin(req.body, 'user', res);
})

router.get('/profile', userAuth, async (req, res) => {
    return res.json(serializeUser(req.user))
})
router.post("/token", async (req, res) => {
    refreshToken(req.body, req, res);
})

module.exports = router;