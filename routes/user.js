const router = require("express").Router();
const { getAllUser,  deleteUser, getUserById  } = require('../controllers/user')

router.get("/", async (req, res) => {
    await getAllUser(req, res)
})

router.get("/:id", async (req, res) => {
    await getUserById(req, res);
})

router.put("/profile/:id", async (req, res) => {
    await updateUser(req, res);
})

router.delete("/:id", async (req, res) => {
    await deleteUser(req, res);
})

module.exports = router;