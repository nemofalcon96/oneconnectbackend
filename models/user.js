const { Schema, model } = require('mongoose');
const UserSchema = new Schema({
    email: {
        type: String,
    },
    username: {
        type: String,
    },
    password: {
        type: String,
    },
}, { timestamps: true }
)

module.exports = model('users', UserSchema);
